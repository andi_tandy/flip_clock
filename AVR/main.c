/*
Author: A.Tandy
Description: Electromechanical Clock Drive
F_CPU =  8000000

Pin Interface:
RTC: I2C
Segment selector: SPI

Unit Selector:
PA0->EN1
PA1->EN2
PA2->EN3
PA3->EN4
PC0->IN1
PC1->IN2
PC2->IN3
PC3->IN4

Button:
BTN0->PD2(INT0)
BTN1->PD3(INT1)

Segment map:
    -----
   |  7  |
    -----
 -         -
| |       | |
|2|       |5|
| |       | |
 -         -
    -----
   |  3  |
    -----
 -         -
| |       | |
|1|       |6|
| |       | |
 -         -
    -----
   |  4  |
    -----
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//Function Prototype
void unit(char a, char b);	//function to turn the high side or low side switch of each unit
void segment (char a, char b);	//function to turn the high side or low side switch of each segment
void SPI_MasterTransmit(char cData);	//Function to trensmit SPI Data
void display_number(char a, char number);	//Function to display number on each unit
void display_segment(char a, char b, char c);	//Function to turn on and off each segment
void clear_unit(char a);	//function to clear a unit
void clear_all(void);	//function to clear all unit

int main(void)
{
	//Initialize Unit Selector
	DDRA |= 0x0F;  // Set PA0&PA1 as output 
	DDRC |= 0x0F;  // Set PC0-3 as output
	
	//Initialize SPI
	DDRD |= 0x03;  // SPI Device selector
	PORTD |= 0x03; //Unselect SPI device
	DDRB |= 0x2C;  // Set MOSI and SCK and SS pin as output
	SPCR |= 0x54;  // Enable SPI, Master, set clock rate fck/4, CPAH 1
	SPSR |= 0x01;  // set clock rate fck/2
	
	//Internal pull up unused pin
	PORTB |= 0xC7;
	PORTC |= 0x80;
	PORTD |= 0xF0;
	
	
	_delay_ms(1000);
	
	clear_all();
	
	_delay_ms(1000);
	
	
	while(1) 
	{
		for(int a = 4; a>=1;a--)
		{
			for(int b = 0; b<=9;b++)
			{
				clear_unit(a);
				display_number(a,b);
				_delay_ms(1000);
			}
		}
		clear_all();
		_delay_ms(1000);
	
	}
return 0; // :P 
}

void unit(char a, char b)
{
	if(b==0)
	{
		switch (a)
		{
			case 1:
				PORTA |=   0x01;
				PORTC &= ~(0x01);
				break;
			case 2:
				PORTA |=   0x02;
				PORTC &= ~(0x02);
				break;				
			case 3:
				PORTA |=   0x04;
				PORTC &= ~(0x04);
				break;				
			case 4:
				PORTA |=   0x08;
				PORTC &= ~(0x08);
				break;				
		}
	}
	else if(b==1)
	{
		switch (a)
		{
			case 1:
				PORTA |= 0x01;
				PORTC |= 0x01;
				break;
			case 2:
				PORTA |= 0x02;
				PORTC |= 0x02;
				break;				
			case 3:
				PORTA |= 0x04;
				PORTC |= 0x04;
				break;				
			case 4:
				PORTA |= 0x08;
				PORTC |= 0x08;
				break;				
		}
	}
	_delay_ms(1);
	PORTA &= ~(0x0F);	//Disable Unit selector
}

void segment(char a, char b)
{
	if(b==0)
	{
		PORTD &= ~(0x02);

	}
	else if(b==1)
	{
		PORTD &= ~(0x01);

	}
	_delay_ms(1);
	switch (a)
	{
		case 0:
			SPI_MasterTransmit(0x00);
			break;
		case 1:
			SPI_MasterTransmit(0x01);
			break;
		case 2:
			SPI_MasterTransmit(0x02);
			break;				
		case 3:
			SPI_MasterTransmit(0x04);
			break;				
		case 4:
			SPI_MasterTransmit(0x08);
			break;
		case 5:
			SPI_MasterTransmit(0x10);
			break;
		case 6:
			SPI_MasterTransmit(0x20);
			break;
		case 7:
			SPI_MasterTransmit(0x40);
			break;
	}
	_delay_ms(1);
	PORTD |= 0x03;
}

void display_segment(char a, char b, char c)
{
	_delay_ms(1);
	if(c==1)
	{
		segment(a,1);
		unit(b,0);
		segment(0,1);
	}
	else if(c==0)
	{
		segment(a,0);
		unit(b,1);
		segment(0,0);
	}
}

void display_number(char a, char number)
{
	switch (number)
	{
		case 0:
			display_segment(1,a,1);
			display_segment(2,a,1);
			display_segment(4,a,1);
			display_segment(5,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 1:
			display_segment(5,a,1);
			display_segment(6,a,1);
			break;
		case 2:
			display_segment(1,a,1);
			display_segment(3,a,1);
			display_segment(4,a,1);
			display_segment(5,a,1);
			display_segment(7,a,1);
			break;
		case 3:
			display_segment(3,a,1);
			display_segment(4,a,1);
			display_segment(5,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 4:
			display_segment(2,a,1);
			display_segment(3,a,1);
			display_segment(5,a,1);
			display_segment(6,a,1);
			break;
		case 5:
			display_segment(2,a,1);
			display_segment(3,a,1);
			display_segment(4,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 6:
			display_segment(1,a,1);
			display_segment(2,a,1);
			display_segment(3,a,1);
			display_segment(4,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 7:
			display_segment(5,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 8:
			display_segment(1,a,1);
			display_segment(2,a,1);
			display_segment(3,a,1);
			display_segment(4,a,1);
			display_segment(5,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
		case 9:
			display_segment(2,a,1);
			display_segment(3,a,1);
			display_segment(5,a,1);
			display_segment(6,a,1);
			display_segment(7,a,1);
			break;
	}
}

void clear_unit(char a)
{
	for(int n = 1;n<=7;n++)
	{
		display_segment(n,a,0);
		_delay_ms(100);
	}
}

void clear_all(void)
{
	for(int n = 1;n<=4;n++)
	{
		clear_unit(n);
	}
}

void SPI_MasterTransmit(char cData)
{
/* Start transmission */
SPDR = cData;
/* Wait for transmission complete */
while(!(SPSR & (1<<SPIF)));
}

void I2C_MasterInit(void)
{
	TWBR = 42;	//Set the I2C SCL to 80kHz
	TWCR |= 0x04; //Enable TWI
}

